function tabs(tabNav, tabContent){
	$(tabContent+':first-child').css('display', 'block');
	$(tabNav).click(function() {
		$(tabNav).removeClass('active');
		$(this).addClass("active");
		$(tabContent).hide();
		var selected_tab = $(this).find("a").attr("href");
		$(selected_tab).fadeIn();
		return false;
	});
}

(function ($) {

	'use strict';

	// MODERNFAMILY-CAROUSEL
	$(".modernfamily-carousel").owlCarousel({
		mouseDrag: true,
		margin: 0,
		touchDrag: true,
		autoplay: true,
		navText: [
			"<i class='fa fa-angle-left fa-2x'></i>",
        	"<i class='fa fa-angle-right fa-2x'></i>"
		],
		nav: true,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			400:{
				items: 2
			},
			600:{
				items: 4
			},
			1000:{
				items: 4
			},
		}
	});

	// CHECKOUT-CAROUSEL
	$(".checkout-carousel").owlCarousel({
		mouseDrag: true,
		margin: 0,
		touchDrag: true,
		navText: [
			"<i class='fa fa-angle-left fa-2x'></i>",
        	"<i class='fa fa-angle-right fa-2x'></i>"
		],
		nav: true,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			400:{
				items: 2
			},
			600:{
				items: 4
			},
			1000:{
				items: 4
			},
		}
	});
	
	// PANELISTFAMILY-CAROUSEL
	$(".panelist-carousel").owlCarousel({
		mouseDrag: true,
		margin: 0,
		touchDrag: true,
		navText: [
			"<i class='fa fa-angle-left fa-2x'></i>",
        	"<i class='fa fa-angle-right fa-2x'></i>"
		],
		nav: true,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			400:{
				items: 2
			},
			600:{
				items: 4
			},
			1000:{
				items: 4
			},
		}
	});	

	// CLONE SUBIMG-EVENTS CONTENT-EVENTS
	// $('.subimg-events > figure').clone(false).appendTo($(".rwd-subimg-events"))

	// CLONE NAVIGATION
	$('.main-navigation > ul').clone(false).find("ul,li").appendTo($(".mobile-nav"));

	// HANDLER RWD-NAVIGATION
	$('.btn-nav').on( 'click', function() {
		$(".mobile-menu").collapse({
			toggle: false
		});
	});
	$(".mobile-nav").on('show.bs.collapse' , function() {
		$("body").on( 'click', function() {
			$(".mobile-nav").collapse("hide");
		});
	});

	// CONFIG ISOTOPE
	// var $container = $(".ins-container");
	// $container.imagesLoaded( function() {
	// 	$container.isotope();
	// });

	// $(".ins-filter a").click( function() {
	// 	var selector = $(this).attr("data-filter");
	// 	$container.isotope({
	// 		itemSelector: ".item.ins",
	// 		filter: selector
	// 	});
	// 	return false;
	// });

	// $(".ins-filter a").click( function (e) {
	// 	$(".ins-filter a").removeClass("active");
	// 	$(this).addClass("active");
	// });

	// $(".btn-rwd-top, .close-nav-right").click( function(e){
		// var nav_side_left = $("#nav-side-left");
		// var nav_side_right = $("#nav-side-right");
		// var wrapper = $(".wrapper-inner");
		// nav_side_right.toggleClass("on-active");
		// nav_side_left.removeClass("on-active");
		// wrapper.toggleClass("on-active-right");
	// });
	
	
	//MAP TABS
	$(".box-desc:first-child").css('display', 'block');
	$(".mapNav li").click(function() {
		$(".mapNav li").removeClass('active');
		$(this).addClass("active");
		$(".box-desc").hide();
		var selected_tab = $(this).find("a").attr("href");
		$(selected_tab).fadeIn();
		return false;
	});

	tabs('#tabs li', '.tab_content');

	/*$("#tabs li").click(function() {
		$("#tabs li").removeClass('active');
		$(this).addClass("active");
		$(".tab_content").hide();
		var selected_tab = $(this).find("a").attr("href");
		$(selected_tab).fadeIn();
		return false;
	});*/

   

	// COMBO SUBSCRIBE-NAME
	$('.menu-inquiries li a').click( function() {
		var value = $(this).text();
		$(this).parents('.btn-group').find('.dropdown-toggle').html(value+ ' <i class="fa fa-caret-down"></i>');
	});


	//FIXED NAV
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1){  
	        $('.site-header').addClass("sticky");
	    }
	    else{
	        $('.site-header').removeClass("sticky");
	    }
	});

	//MODAL 
	var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");
	  $('a[data-modal-id]').click(function(e) {
	    e.preventDefault();
	    $("body").append(appendthis);
	    $('.wrapper').css({
	    	'-webkit-filter': 'blur(5px)',
			  '-moz-filter': 'blur(5px)',
			  '-o-filter': 'blur(5px)',
			  '-ms-filter': 'blur(5px)',
			  'filter': 'blur(5px)'
	    });
	    $(".modal-overlay").fadeTo(500, 0.7);
	    //$(".js-modalbox").fadeIn(500);
	    var modalBox = $(this).attr('data-modal-id');
	    $('#'+modalBox).fadeIn($(this).data());
	  });  
	  
	  
	$(".js-modal-close, .modal-overlay").click(function() {
	    $(".modal-box, .modal-overlay").fadeOut(500, function() {
	        $(".modal-overlay").remove();
	    });
	     $('.wrapper').css({
	    	'-webkit-filter': 'blur(0)',
			  '-moz-filter': 'blur(0)',
			  '-o-filter': 'blur(0)',
			  '-ms-filter': 'blur(0)',
			  'filter': 'blur(0)'
	    });
	});
	 
	$(window).resize(function() {
	    $(".modal-box").css({
	        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
	        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
	    });
	});
	 
	$(window).resize();

	/*$(window).load(function(){
		if($('body').hasClass('.modal-open')){
			$('.wrapper').css({
				'-webkit-filter': 'blur(5px)',
				'-moz-filter': 'blur(5px)',
				'-o-filter': 'blur(5px)',
				'-ms-filter': 'blur(5px)',
				'filter': 'blur(5px)'
		    });
		}
	});*/

	$(window).load(function(){
		if($('#adopext-modal').length){
			$('.wrapper').css({
				'-webkit-filter': 'blur(5px)',
				'-moz-filter': 'blur(5px)',
				'-o-filter': 'blur(5px)',
				'-ms-filter': 'blur(5px)',
				'filter': 'blur(5px)'
		    });
		}
	});
	$(window).resize(function() {
	    $("#adopext-modal .page-header").css({
	        top: ($(window).height() - $("#adopext-modal .page-header").outerHeight()) / 2,
	        left: ($(window).width() - $("#adopext-modal .page-header").outerWidth()) / 2
	    });
	}).resize();

	$(window).load();

	$(window).resize(function(){
		if($(window).width() < 571){
			//$('.mobile-nav').append("<li><p>Test Button</p></li>");
		}
	}).resize();

  	/*$("#bars li .bar").each(function() { 
	   var percentage = $(this).data('percentage');        
	   $(this).animate({
	   		'height' : percentage + '%'
	   }, 2000);  
	});*/

	function graph(){
		$(".bars li .bar").each(function() { 
			var percentage = $(this).data('percentage');        
		   $(this).stop().animate({
		   		'height' : percentage + '%'
		   }, 1500); 
		});
	}

	$('#tabs li').on('click', function(){
		$(".bars li .bar").css('height', 0);
	    graph();
	});
	$(window).on('load', function(){
		graph();
	});

})(jQuery);